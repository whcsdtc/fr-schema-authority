import frSchema from "@/outter/fr-schema/src"

const {
    schemaFieldType
} = frSchema

function validateString(str) {
    // 使用正则表达式进行匹配
    const regex = /^(?=.*\d)(?=.*[a-zA-Z])(?=.*[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]).{8,}$/;
    return regex.test(str);
}
export default {
    loginid: {
        title: "用户名",
        required: true,
        sorter:true,
        readOnly: true,
        searchPrefix: "like"
    },
    name: {
        title: "姓名",
        required: true,
        sorter:true,
        searchPrefix: "like"
    },


    enable: {
        sorter: true,
        search: false,

        title: "是否启用",
        type: schemaFieldType.Select,
        dict: {
            true: {
                value: "true",
                default: true,
                remark: "是",
            },
            false: {
                value: "false",
                remark: "否",
            },
        },
        render: (item, props) => {
            if (props.enable) {
                return "是"
            } else {
                return "否"
            }
        },
    },
    password: {
        title: "密码",
        required: true,
        listHide: true,
        editHide: true,
        type: schemaFieldType.Password,
        search: false,
        decoratorProps: {
            rules: () => ({
                validator(_, value) {
                    try {
                        if(!validateString(value)){
                            return  Promise.reject("密码长度需要大于等于八位，并包含字母数字特殊字符。")
                        }
                        return Promise.resolve()
                    } catch (error) {}
                },
            }),
        },
    },
    department_key: {
        title: "部门",
        sorter: true,
        searchPrefix: "cs",
        type: schemaFieldType.MultiSelect,
        // props:{
        //     mode:"multiple"
        // },
        search: true,

    },
    role_id: {
        sorter: true,
        title: "角色",
        addHide: true,
        infoHide: true,
        editHide: true,
        search: false,

        type: schemaFieldType.MultiSelect,
        dictFunc: (dict) => {
            return dict.role
        },
    },
    mobile_phone: {
        title: "电话",
        search: true,
        sorter:true,
        searchPrefix: "like"
    },
    email: {
        title: "邮箱",
        search: true,
        sorter:true,
        searchPrefix: "like"

    },
    address: {
        title: "地址",
        listHide: true,
        search: false,

    },
}
