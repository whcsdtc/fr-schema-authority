import { schemaFieldType } from "@/outter/fr-schema/src/schema"
import React from 'react';
import { createApi } from '@/outter/fr-schema/src/service';

/**
 * 项目
 */
const schema = {
    loginid: {
        title: '用户名',
        required: true,
        fixed: 'left',
        sorter: true,
    },
    name: {
        title: '姓名',
        required: true,
        fixed: 'left',
        sorter: true,
        // readOnly: true,
        render: (item, data) => {
            if (data.sum_item * 0.1 > data.sum_ready_items) {
                return <span style={{ color: 'red' }}>{item}</span>;
            }
            return <span>{item}</span>;
        },
    },
    password: {
        title: '密码',
        required: true,
        fixed: 'left',
        sorter: true,
        type: schemaFieldType.Password,
    },
    confirm: {
        title: '确认密码',
        required: true,
        type: schemaFieldType.Password,
        fixed: 'left',
        sorter: true,
    },
    mobile_phone: {
        title: '电话',
        fixed: 'left',
        sorter: true,
    },
    email: {
        title: '邮箱',
        fixed: 'left',
        sorter: true,
    },

};

// 会议室
const project = createApi('user_auth/user/register', schema, null, 'eq.');

export default {
    schema,
    service: project,
};
