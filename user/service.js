import frSchema from '@/outter/fr-schema/src';
import schema from './schema';

const { createApi, createBasicApi, oauth, utils } = frSchema;

// 用户
const service = createApi('user_auth/user', schema, {}, 'eq.');
service.get = async (args) => {
    const response = await createApi('user_auth/user_extend', schema, {}, 'eq.').get(args);
    let list  = response.list.map((item)=>{
        return {...item, department_key: item.department_key|| []}
    })
    return {...response, list};
};

createApi('user_auth/user_extend', schema, {}, 'eq.').get;


const convertRole = (item) => {
    return {
        ...item,
    };
};
/**
 * 登录
 * @param params
 * @returns {Promise<ClientOAuth2.Token>}
 */
service.login = (params) => {
    const client = oauth();
    return client.owner.getToken(params.userName, params.password).catch((e) => {
        throw new Error('登录出错');
    });
};

/**
 * 查询当前用户
 * @returns {Promise<void>}
 */
service.post = async (args) => {
    if(args.enable === "true"){
        args.enable = true
    }else{
        args.enable = false
    }
    const response = createApi('user_auth/user', schema, {}, 'eq.').post(args);
    return response;
};
service.getDetail = async (args) => {
    const response = createApi('user_auth/user', schema, {}, 'eq.').getDetail(args);
    let list  = response.list.map((item)=>{
        return {...item, department_key: item.department_key|| []}
    })
    return response;
};

service.queryCurrent = async () => {
    const response = await createBasicApi('user_auth/user/current').get();
    return response;
};

/**
 * 查询角色信息
 * @returns {Promise<void>}
 */

service.queryRoleList = async (params) => {
    const { list, ...others } = await createApi(`user_auth/user/role`, schema).get(params);
    return {
        list: list.map((item) => convertRole(item)),
        ...others,
    };
};

service.enableMulti = createApi(`user_auth/user`, schema, {}, '').patch

service.editRole = async (params) => {
    const res = await createApi(`user_auth/user/role`).put(params);
    return res;
};

service.editPwd = async (params) => {
    const res = await createApi(`user_auth/user/password/reset`, '', '', '').post({
        ...params,
        new_password: params.password,
    });
    return res;
};

service.editMyPwd = async (params) => {
    const res = await createApi(`user_auth/user/password`, '', '', '').patch(params);
    return res;
};


export default service;
