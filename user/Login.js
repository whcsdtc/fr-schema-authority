import {Alert} from "antd"
import {formatMessage, FormattedMessage, injectIntl} from "umi"
import React, {Component} from "react"
import {
    AlipayCircleOutlined,
    LockOutlined,
    MobileOutlined,
    TaobaoCircleOutlined,
    UserOutlined,
    WeiboCircleOutlined,
  } from '@ant-design/icons';
import {connect} from "dva"
import LoginComponents from "./components/Login"
import styles from "./Login.less"
import LicenseUpload from "@/pages/authority/permission/license/LicenseUpload"
import Register from "./components/Register/Register"
import { ProFormText } from '@ant-design/pro-form';
import configService from '../config/service';

const config = SETTING

const { Tab, UserName, Password, Mobile, Captcha, Submit } = LoginComponents

@connect(({ login, loading, global, user }) => ({
    userLogin: login,
    global: global,
    user: user,
    submitting: loading.effects["login/login"],
}))
class Login extends Component {
    loginForm = undefined
    state = {
        type: "account",
        autoLogin: true,
        register: false
    }

    async componentDidMount(){
        let data = await configService.get({limit: '1000'})
        let result = data.list.find((item)=>{return item.key==="register_switch"})
        let register_switch = (result && result.value === "true")
        this.setState({
            register: register_switch
        })
    }

    changeAutoLogin = (e) => {
        this.setState({
            autoLogin: e.target.checked,
        })
    }
    handleSubmit = (values) => {
        const {type} = this.state
        const {dispatch} = this.props
        dispatch({
            type: "login/login",
            payload: {...values, type},
        })
    }
    onTabChange = (type) => {
        this.setState({
            type,
        })
    }
    onGetCaptcha = () =>
        new Promise((resolve, reject) => {
            if (!this.loginForm) {
                return
            }

            this.formRef.current
                .validateFields()
                .then(async values => {
                    const {dispatch} = this.props
                    try {
                        const success = await dispatch({
                            type: "login/getCaptcha",
                            payload: values.mobile,
                        })
                        resolve(!!success)
                    } catch (error) {
                        reject(error)
                    }
                })
                .catch(err => {
                    console.log("err", err)
                })
        })
    renderMessage = (content) => (
        <Alert
            style={{
                marginBottom: 24,
            }}
            message={content}
            type="error"
            showIcon
        />
    )

    renderRegister(){
        const { showRegister } = this.state
        return (
            showRegister && (
                <Register
                    onCancel={() =>
                        this.setState({
                            showRegister: false,
                        })
                    }
                />
            )
        )
    }

    renderLicenseUpload() {
        const { showLicenseUpload } = this.state
        return (
            showLicenseUpload && (
                <LicenseUpload
                    onCancel={() =>
                        this.setState({
                            showLicenseUpload: false,
                        })
                    }
                />
            )
        )
    }

    render() {
        const { userLogin, submitting } = this.props
        const { status, type: loginType } = userLogin
        const { type, autoLogin } = this.state
        const {register} =this.state
        return (
            <div className={styles.main}>
                <LoginComponents
                    defaultActiveKey={type}
                    onTabChange={this.onTabChange}
                    onSubmit={this.handleSubmit}
                    onCreate={(form) => {
                        this.loginForm = form
                    }}
                >
                    <div className={styles.other}>
                    <ProFormText
                        name="userName"
                        fieldProps={{
                            size: 'large',
                            prefix: <UserOutlined className={styles.prefixIcon} />,
                        }}
                        placeholder={"用户名"}
                        rules={[
                        {
                            required: true,
                            message: (
                            <FormattedMessage
                                id="pages.login.username.required"
                                defaultMessage="请输入用户名!"
                            />
                            ),
                        },
                        ]}
                    />
                    <ProFormText.Password
                        name="password"
                        fieldProps={{
                        size: 'large',
                            prefix: <LockOutlined className={styles.prefixIcon} />,
                        }}
                        placeholder={"密码"}
                        onPressEnter={(e) => {
                            e.preventDefault()

                            if (this.loginForm) {
                                this.loginForm.current
                                    .validateFields()
                                    .then(async fieldsValue => {
                                        this.handleSubmit(fieldsValue)
                                    })
                                    .catch(err => {
                                        console.log("err", err)
                                    })
                            }
                        }}
                        rules={[
                        {
                            required: true,
                            message: (
                            <FormattedMessage
                                id="pages.login.password.required"
                                defaultMessage="请输入密码！"
                            />
                            ),
                        },
                        ]}
                    />
                    </div>
                    <Submit loading={submitting}>
                        <FormattedMessage id="user-login.login.login" />
                    </Submit>
                    <div className={styles.other} >
                        <div
                            onClick={() => {
                                this.setState({ showLicenseUpload: true })
                            }}
                            style={{display: 'inline-block',marginLeft: '10px', float: 'right'}}
                        >
                            <a className={styles.register}>证书上传</a>
                        </div>
                        {
                            register&& <div
                            onClick={() => {
                                this.setState({ showRegister: true })
                            }}
                            style={{display: 'inline-block',  float: 'right'}}
                        >
                            <a className={styles.register}>用户注册</a>
                        </div>
                        }
                        
 
                    </div>
                </LoginComponents>
                {this.renderLicenseUpload()}
                {this.renderRegister()}

            </div>
        )
    }
}

export default injectIntl(Login)
