// 证书
import { createApi, createBasicApi } from '@/outter/fr-schema/src/service';

const license = {};

// 判断系统是否注册
license.getIsRegistered = createApi('user_auth/license/check', null, {
    skipOauth: true,
}).getBasic;

// 返回注册码
license.getMachieCode = createApi('user_auth/license', null, {
    skipOauth: true,
}).getBasic;

// 上传注册文件
license.post = async (args) => {
    const response = await createApi('user_auth/license', null, {
        skipOauth: true,
    }).post({license: args.license});

    return response.data;
};

license.patch = async (args) => {
    const response = await createApi('user_auth/license', null, {
        skipOauth: true,
    }).post({license: args.license});

    return response.data;
};


license.get =async (args)=>{ 
    
    let data =await createApi('user_auth/license', null, {
    skipOauth: true }).getBasic({
        ...args, 
    })
    let list = []
    if(data.data.license){
        list = data.data.license.map((item, index)=>{
            return {...item, register_info:data.data.register_info}
        })
    }
    return {...data, list}
}

export default license;
