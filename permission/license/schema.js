import { schemaFieldType } from "@/outter/fr-schema/src/schema";
import {Typography} from "antd"
const { Paragraph } = Typography;
export default {
    machine_info: {
        title: '机器码',
        readOnly: true,
        editHide: true,
        search: false,

    },
    register_info: {
        title: '注册信息',
        readOnly: true,
        type: schemaFieldType.TextArea,
        showHide:true,
        hideInTable: true,
        listHide: true,
        props: {
            autoSize:true,
        },
        renderInput: (item, data)=>{
            return <div style={{width: '300px', marginTop: '5px'}}><Paragraph copyable={{ tooltips: true }}>{data.register_info}</Paragraph></div>
        },
        search: false
    },
    product_key: {
        title: '产品编码',
        readOnly: true,
        search: false,
        editHide: true
    },
    custom_name: {
        title: '客户名称',
        readOnly: true,
        search: false,
        editHide: true

    },

    avaiable: {
        title: '是否有效',
        type: schemaFieldType.Select,
        dict: {
            true: {
                value: 'true',
                remark: '是'
            },
            false: {
                value: 'false',
                remark: '否'
            }
        },
        readOnly: true,
        search: false,
        editHide: true
    },
    registered_time: {
        title: '注册时间',
        search: false,
        editHide: true

    },
    due_time: {
        title: '失效时间',
        search: false,
        editHide: true
    },
    license: {
        title: '证书',
        // type: 'Upload',
        hideInTable: true,
        listHide:true,
        search: false,
        extra: '使用注册信息提供给厂商申请证书。',
        type: schemaFieldType.TextArea,
        showHide: true,
        required: true,
        extra:"使用注册信息提供给厂商申请证书。",
        props: {
            autoSize:{
                minRows: 6,
                maxRows: 6
            },
            // autoSize: true,
        },
    },
    // file: {
    //     title: '证书',
    //     type: 'Upload',
    //     showHide: false,
    //     required: true,
    // },
};
