import React, { PureComponent } from 'react';
import '@ant-design/compatible/assets/index.css';
import { message, Modal, Spin, Skeleton } from 'antd';
import service from './service';
import schema from './schema';
import frSchemaUtils from '@/outter/fr-schema-antd-utils/src';
import actions from '@/outter/fr-schema/src/actions';
const config = SETTING

const { InfoModal } = frSchemaUtils.components;

/**
 * handleAdd 提交后回调
 * onCancel 取消时回调
 */
class LicenseUpload extends PureComponent {
    state = {
        loading: true,
        machine_info: null,
        file: null,
    };

    componentDidMount() {
        const { product_key } = config
        this.setState({ loading: true }, async () => {
            const res = await service.get();
            this.setState({
                loading: false,
                data: {
                    register_info: res.register_info || (res.data && res.data.register_info),
                },
            });
        });
    }

    handleSubmit = async args => {
        try {
            await service.post(args);
            message.success('上传成功');
            this.props.onCancel && this.props.onCancel();
            this.props.handleAdd && this.props.handleAdd(formData);
        } catch (error) {
            message.error(error.message)
        }

    };

    getSchema() {
        const { license, register_info } = schema;
        return { register_info, license };
    }

    render() {
        const { data, loading } = this.state;
        return loading? (
            <Modal getContainer={document.getElementsByClassName("ant-pro-table").length ? () =>{
                let len = document.getElementsByClassName("ant-pro-table").length
                return document.getElementsByClassName("ant-pro-table")[len-1]
            }: undefined} visible={true} width={700} footer={null} title={'证书提交'}>
                <Skeleton />
            </Modal>
        ) : (
            <InfoModal
                title="证书提交"
                visible={true}
                schema={this.getSchema()}
                action={actions.edit}
                values={data}
                handleUpdate={this.handleSubmit}
                {...this.props}
            />
        );
    }
}

export default LicenseUpload;
