import schema from "./schema"
import service from "./service"
import frSchemaUtils from '@/outter/fr-schema-antd-utils/src';

const { DataList, Authorized } = frSchemaUtils.components;
import {Button} from 'antd'

class List extends DataList {
    constructor(props) {
        super(props, {
            schema,
            service,
            readOnly:true,
            addHide: true,
            search: false,
            showDelete: false,
            infoProps: {
                offline: true,
            },
        })
    }
    renderOperationExtend(){
        return <Button
            onClick={() =>
                this.handleVisibleModal(true, {
                    register_info: this.state.data.data.register_info

                }, "edit")
            }
        >
            证书上传
        </Button>
    }
}

export default List
