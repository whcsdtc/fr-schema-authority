import schema from '@/schemas/config/schema';
import service from './service';
import frSchemaUtils from '@/outter/fr-schema-antd-utils/src';
const { DataList, ListPage } = frSchemaUtils.components;

class List extends DataList {
    constructor(props) {
        super(props, {
            schema,
            service,
            queryArgs: { ...props.queryArgs, order: 'key.asc' },
            addHide: true,
            // infoProps: {
            //     offline: true,
            // },
            showDelete: false,
        });
    }

    handleVisibleModal = (flag, record, action) => {
        if (record && record.key === 'routes') {
            this.schema.value.type = 'AceEditor';
            this.schema.value.props = {
                style: { width: '350px' },
                height: '300px',
            };
            this.schema.key.props = {
                style: { width: '350px' },
            };
            this.schema.name.props = {
                style: { width: '350px' },
            };
            this.schema.remark.props = {
                style: { width: '350px' },
            };
        } else {
            this.schema.value.type = undefined;
            this.schema.value.props = {};
            this.schema.key.props = {};
            this.schema.name.props = {};
            this.schema.remark.props = {};
        }
        this.setState({
            visibleModal: !!flag,
            infoData: record,
            action,
        });
    };
}

export default List;
