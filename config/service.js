import { createApi } from '@/outter/fr-schema/src/service';
import schema from '@/schemas/config/schema';


let service = createApi('user_auth/config', schema, null, 'eq.');

service.getDetail = async (args) => {
    let data = await createApi('user_auth/config', schema, null, 'eq.').getDetail(
        args,
        {},
        { Authorizations: false },
    );
    return data;
};

export default service;
