import frSchema from '@/outter/fr-schema/src';

const {
    schemaFieldType
} = frSchema;

export default {
    client_name: {
        title: '名称',
        required: true,
        sorter: true,
        searchPrefix: 'like',
        // search: false

    },
    client_id: {
        title: '客户端(标识)ID',
        sorter: true,
        // editHide: true,
        readOnly: true,
        addHide: true,
        required: true,
        search: false

    },
    scope: {
        title: '权限范围',
        sorter: true,
        required: true,
        type: schemaFieldType.Select,
        search: false,

        dict: {
            profile: {
                value: 'profile',
                remark: 'profile',
            },
        },
    },
    response_type: {
        title: '授权类型',
        search: false,

        sorter: true,
        required: true,
        type: schemaFieldType.Select,
        dict: {
            code: {
                value: 'code',
                remark: 'code',
            },
        },
    },
    client_secret: {
        title: '密钥',
        listHide: true,
        search: false,

        addHide: true,
        // editHide: true,
        readOnly: true,
        required: true,
    },
    grant_type: {
        title: '授权模式',
        search: false,

        required: true,
        // listHide: true,
        type: schemaFieldType.MultiSelect,
        dict: {
            // authorization_code: {
            //     value: 'authorization_code',
            //     remark: '授权码模式',
            // },
            password: {
                value: 'password',
                remark: '密码模式',
            },
            refresh_token: {
                value: 'refresh_token',
                remark: '刷新令牌',
            },
            // implicit: {
            //     value: 'implicit',
            //     remark: '简化模式',
            // },
            client_credentials: {
                value: 'client_credentials',
                remark: '客户端模式',
            },
        },
    },
};
